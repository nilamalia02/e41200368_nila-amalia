import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'data/api_provider.dart';
import 'model/popular_movies.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Movies App Nila',
      theme: ThemeData.dark(),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ApiProvider apiProvider = ApiProvider();
  Future<PopularMovies> popularMovies;

  String imageBaseUrl = 'https://image.tmdb.org/t/p/w500';

  @override
  void initState() {
    super.initState();
    popularMovies = apiProvider.getPopularMovies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movies App Nila'),
      ),
      body: Container(
        child: FutureBuilder(
          future: popularMovies,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              print("Has Data: ${snapshot.hasData}");
              return ListView.builder(
                itemCount: snapshot.data.results.length,
                itemBuilder: (BuildContext context, int index) {
                  return moviesItem(
                      poster:
                          '$imageBaseUrl${snapshot.data.results[index].posterPath}',
                      title: '${snapshot.data.results[index].title}',
                      date: '${snapshot.data.results[index].releaseDate}',
                      overview: '${snapshot.data.results[index].overview}',
                      voteAverage:
                          '${snapshot.data.results[index].voteAverage}',
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => MovieDetail(
                                  movie: snapshot.data.results[index],
                                )));
                      });
                },
              );
            } else if (snapshot.hasError) {
              print("Has Error: ${snapshot.hasError}");
              return Text('Error woy!!!');
            } else {
              print("Loading...sabar...");
              return CircularProgressIndicator();
            }
          },
        ),
      ),
    );
  }

  Widget moviesItem(
      {String poster,
      String overview,
      String title,
      String date,
      String voteAverage,
      Function onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.all(10),
        child: Card(
          child: Container(
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: 120,
                    child: Image(
                      image: CachedNetworkImageProvider(poster),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                      child: Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                title,
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w600),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Column(
                                children: <Widget>[
                                  Text(
                                    overview,
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Color.fromARGB(255, 209, 206, 206),
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 10,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(children: <Widget>[
                                Icon(
                                  Icons.calendar_today,
                                  size: 11,
                                  color: Colors.blue,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(date,
                                    style: TextStyle(
                                        fontSize: 11,
                                        color: Color.fromARGB(
                                            255, 209, 206, 206))),
                              ]),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.star,
                                    size: 12,
                                    color: Colors.yellow,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(voteAverage,
                                      style: TextStyle(
                                        fontSize: 12,
                                      )),
                                ],
                              ),
                            ],
                          )))
                ]),
          ),
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 5,
          margin: EdgeInsets.all(10),
        ),
      ),
    );
  }
}

class MovieDetail extends StatelessWidget {
  final Results movie;

  const MovieDetail({Key key, this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(movie.title),
      ),
      body: Container(
        child: Text(
          movie.overview,
          style: TextStyle(fontSize: 18),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
