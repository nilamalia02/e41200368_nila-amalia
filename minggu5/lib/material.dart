import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.dashboard),
          title: Text("Nila Belajar MaterialApp Scaffold"),
          actions: <Widget>[
            Icon(Icons.search),
            // Icon(Icons.find_in_page)
          ],
          actionsIconTheme: IconThemeData(color: Colors.yellow),
          backgroundColor: Color.fromARGB(255, 255, 0, 0),
          bottom: PreferredSize(
              child: Container(
                color: Color.fromARGB(255, 0, 255, 170),
                height: 4.0,
              ),
              preferredSize: Size.fromHeight(4.0)),
          centerTitle: true,
        ),

        //PERUBAHAN BARU
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color.fromARGB(255, 0, 4, 224),
          child: Text('+'),
          onPressed: () {},
        ),
        body: null,
        //PERUBAHAN BARU
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
