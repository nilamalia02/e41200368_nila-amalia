import 'dart:ffi';

void main() {
//1. Operator Aritmatika
  var x = 16;
  var y = 12;
  print(x + y);
  print(x - y);
  print(x * y);
  print(x / y);
  print(x ~/ y);
  print(x % y);

//2. Operator Assignment (=)
  var angka;
  angka = 10; // Contoh assignment variable angka dengan nilai 10

//3. Operator Perbandingan
  //Equals Operator(==)
    var angka1 = 100;
    print(angka1 == 100); // true
    print(angka1 == 20); // false
  //Not Equal(!=)
    var sifat = "rajin";
    print(sifat != "malas"); // true
    print(sifat != "bandel"); //true
  //Strict Equal ( === )
    var angka2 = 8;
    print(angka2 == "8"); // true, padahal "8" adalah string.
    //print(angka1 === "8"); // false, karena tipe data nya berbeda
    //print(angka1 === 8); // true
  //Kurang dari & Lebih Dari ( <, >, <=, >=)
    var number = 17;
    print(number < 20); // true
    print(number > 17); // false
    print(number >= 17); // true, karena terdapat sama dengan
    print(number <= 20); // true

//4. Operator Kondisional
  //OR ( || )
    print(true || true); // true
    print(true || false); // true
    print(true || false || false); // true
    print(false || false); // false
  //AND ( && )
    print(true && true); // true
    print(true && false); // false
    print(false && false); // false
    print(false && true && true); // false
    print(true && true && true); // true

//String
  var sentences = "dart";
  print(sentences[0]); // "d"
  print(sentences[2]); // "r"

//Numbers
  // declare an integer
  int num1 = 10;
  // declare a double value
  double num2 = 10.50;
  // print the values
  print(num1); //10
  print(num2); //10.5

//Mengubah dari int ke string
  int j = 45;
  String t = "$j";
  print("hello" + t);

}

