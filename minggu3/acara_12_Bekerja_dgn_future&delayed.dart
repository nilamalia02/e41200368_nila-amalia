Future delayedPrint(int seconds, String message) {
  final duration = Duration(seconds: seconds);
  return Future.delayed(duration).then((value) => message);
}

main(List<String> args) {
  print('nila');
  delayedPrint(5, 'nila').then((status) {
    print(status);
  });
  print("is");
}
