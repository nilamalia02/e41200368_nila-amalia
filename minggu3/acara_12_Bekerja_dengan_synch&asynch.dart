void main(List<String> args) async {
  var t = Titan(); //inisialisasi t = object dari class titan

  print("zeke"); //mencetak zake
  print(t.name); //akan mencetak string name yang pertama yaitu eren yeger
  await t.getName(); //masuk kedalam async await untuk mendelayed object di dalamnya
//dan akan mencetak get name [done]
  print(t.name); //mencetak grisha yeger karena sudah di masukkan kedalam async nya
  print("rener"); //akan mencetak rener
}

class Titan {
  String name = "Eren Yeger"; //mengisi value name eren yeger
  Future<void> getName() async { //masuk kedalam Future dan object get name dan inisialisasi async
    await Future.delayed(Duration(seconds: 5)); //memberi delayed pada object selama 5 detik
    name = "grisha"; //set name gerisha
    print("get name [done]"); //cetak print get name done
  }
}
